all: login build push

login:
	buildah login registry.gitlab.com

build:
	buildah bud -t registry.gitlab.com/1h0/debian-texlive-full:latest .

push:
	buildah push registry.gitlab.com/1h0/debian-texlive-full
