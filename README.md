# Debian Texlive Full

> A Container image based on Debian 12 with `texlive-full` installed to build LaTeX files.

## Building

To build the image with `buildah`, run the following commands:

1. `buildah login registry.gitlab.com`
2. `build bud -t registry.gitlab.com/1h0/debian-texlive-full .`
3. `buildah push registry.gitlab.com/1h0/debian-texlive-full`
